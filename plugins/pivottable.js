import Vue from 'vue'
import { VuePivottable, VuePivottableUi } from 'vue-pivottable'
import 'vue-pivottable/dist/vue-pivottable.css'

Vue.component('vue-pivottable', {
  extends: VuePivottable
})
Vue.component('vue-pivottable-ui', {
  extends: VuePivottableUi
})
